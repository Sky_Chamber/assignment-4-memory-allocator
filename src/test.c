#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define CAPACITY_BYTES 8192
#define ALLOC_AMOUNT 1024
#define BLOC_AMOUNT 80000

static struct block_header* get_block_by_address(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}
void* map_pages(void const* addr, size_t length) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0 );
}

void test1(){
    block_size capacity;
    capacity.bytes = CAPACITY_BYTES;

    void *h = heap_init(capacity_from_size(capacity).bytes);
    if (h == NULL){
        fprintf(stderr, "Test 1 failed");
        return;
    }
    debug_heap(stdout, h);

    void *block = _malloc(256);
    if (block == NULL){
        fprintf(stderr, "Test 1 failed");
        return;
    }
    _free(block);

    munmap(h, REGION_MIN_SIZE);
    printf("Test 1 passed");
}


void test2() {
    void* heap = heap_init(CAPACITY_BYTES);

    if(!heap){
        fprintf(stderr, "Test 2 failed");
        return;
    }

    debug_heap(stdout, heap);

    void* alloc1 = _malloc(ALLOC_AMOUNT);
    void* alloc2 = _malloc(ALLOC_AMOUNT);

    if(!alloc1 || !alloc2){
        fprintf(stderr, "Test 2 failed");
        _free(alloc1);
        _free(alloc2);
        return;
    }

    debug_heap(stdout, heap);
    _free(alloc1);
    debug_heap(stdout, heap);

    munmap(HEAP_START, CAPACITY_BYTES);
    printf("Test 2 passed");
}

void test3() {
    void *heap = heap_init(CAPACITY_BYTES);

    void *alloc1 = _malloc(ALLOC_AMOUNT);
    void *alloc2 = _malloc(ALLOC_AMOUNT);
    void *alloc3 = _malloc(ALLOC_AMOUNT);
    void *alloc4 = _malloc(ALLOC_AMOUNT);

    if (alloc1 == NULL || alloc2 == NULL || alloc3 == NULL || alloc4 == NULL){
        fprintf(stderr, "Test 3 failed");
        _free(alloc1);
        _free(alloc2);
        _free(alloc3);
        _free(alloc4);
        return;
    }

    _free(alloc1);
    _free(alloc2);
    debug_heap(stdout, heap);

    _free(alloc3);
    _free(alloc4);
    debug_heap(stdout, heap);
    
    printf("Test 3 passed");
}

void test4() {
    struct block_header* heap = (struct block_header*) heap_init(CAPACITY_BYTES);

    void *data1 = _malloc(BLOC_AMOUNT);
    void *data2 = _malloc(BLOC_AMOUNT + 1000);
    void *data3 = _malloc(2000);

    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        fprintf(stderr, "Test 4 failed");
        _free(data1);
        _free(data2);
        _free(data3);
        return;
    }

    _free(data3);
    _free(data2);

    debug_heap(stdout, heap);

    struct block_header *block1 = get_block_by_address(data1);
    struct block_header *block2 = get_block_by_address(data2);

    if ((uint8_t *)block1->contents + block1->capacity.bytes != (uint8_t*) block2){
        fprintf(stderr, "Test 4 failed");
        _free(data1);
        _free(data2);
        _free(data3);
        return;
    }

    _free(data1);
    _free(data2);
    _free(data3);

    printf("Test 4 passed");
}

void test5() {
    struct block_header* heap = (struct block_header*) heap_init(BLOC_AMOUNT);

    void *data1 = _malloc(50000);
    if (data1 == NULL) {
        fprintf(stderr, "Test 5 failed");
        _free(data1);
        return;
    }

    struct block_header *address = heap;
    while (address->next != NULL) {
        address = address->next;
    }

    map_pages((uint8_t*) address + size_from_capacity(address->capacity).bytes, 1024);

    void *data2 = _malloc(100000);

    debug_heap(stdout, heap);

    struct block_header *block2 = get_block_by_address(data2);

    if (block2 == address) {
        fprintf(stderr, "Test 5 failed");
        _free(data1);
        _free(data2);
        return;
    }

    _free(data1);
    _free(data2);
    
    printf("Test 5 passed");
}

void run_tests(){
    test1();
    test2();
    test3();
    test4();
    test5();
}

